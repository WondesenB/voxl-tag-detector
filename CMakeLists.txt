cmake_minimum_required(VERSION 3.3)
project(voxl-tag-detector)


set(CMAKE_C_FLAGS "-std=gnu99 ${CMAKE_C_FLAGS}")
set(CMAKE_INSTALL_LIBDIR "/usr/lib64")
set(CMAKE_INSTALL_PREFIX "/usr")

add_subdirectory(apriltag)

set(CMAKE_C_FLAGS "-g -std=gnu99 -Wall -Wextra -Wuninitialized \
	-Wunused-variable -Wdouble-promotion -Wmissing-prototypes \
	-Wmissing-declarations -Werror=undef -Wno-unused-function ${CMAKE_C_FLAGS}")

# so far not necessary!
set(CMAKE_C_FLAGS "-g -O3 -ffast-math -ftree-vectorize ${CMAKE_C_FLAGS}")

set(CMAKE_CXX_FLAGS "-g -O3 -std=gnu++11 -Wall -Wextra -Wuninitialized \
	-Wunused-variable -Wdouble-promotion \
	-Wmissing-declarations -Werror=undef -Wno-unused-function ${CMAKE_CXX_FLAGS}")

# server and client binaries
add_subdirectory(server)
add_subdirectory(clients)

