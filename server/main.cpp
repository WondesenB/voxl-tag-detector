/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <iostream>

#include <opencv2/core/mat.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp> // for imwrite

#include <apriltag.h>
#include <tag36h11.h>
#include <apriltag_pose.h>

#include <modal_pipe_server.h>
#include <modal_pipe_client.h>
#include <modal_start_stop.h>
#include <modal_pipe_interfaces.h>
#include <voxl_cutils.h>

#include "cCharacter.h"
#include "config_file.h"
#include "locations.h"
#include "undistort.h"

using namespace std;
using namespace cv;

#define DET_CH				MAX_CH
#define PROCESS_NAME		"voxl-tag-detector"

// locations loaded from the apriltag_locations.conf file
static tag_location_t locations[MAX_TAG_LOCATIONS];
static int n_locations;
static float default_size;

// lens/distortion stuff
static Mat camMatrix[MAX_CH];
static Mat distCoeffs[MAX_CH];
static int is_fisheye[MAX_CH];
static int has_map_been_computed[MAX_CH];
static undistort_map_t mcv_map[MAX_CH];

// image buffers for rectified images
static uint8_t* img_rect[MAX_CH];

// UMich apriltag things
apriltag_family_t* tf;
apriltag_detector_t* td[MAX_CH];

// command line flags
static int en_debug = 0;
static int en_timing = 0;
static int n_skipped[MAX_CH] = {0,0,0};

// timing averages
static double avg_undistortion_time_ms = 6.0;
static double min_undistortion_time_ms = 10000;
static double avg_detection_time_ms = 8.0;



// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
This is meant to run in the background as a systemd service, but can be\n\
run manually with the following debug options\n\
\n\
-c, --config                only parse the config file and exit, don't run\n\
-d, --debug                 enable debug prints\n\
-h, --help                  print this help message\n\
-t, --timing                enable timing debug prints\n\
\n");
	return;
}

static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"config",			no_argument,		0, 'c'},
		{"debug",			no_argument,		0, 'd'},
		{"help",			no_argument,		0, 'h'},
		{"timing",			no_argument,		0, 't'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "cdht", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			if(config_file_read()){
				exit(-1);
			}
			tag_location_read_file(TAG_LOCATION_PATH, locations, &n_locations, MAX_TAG_LOCATIONS, &default_size);
			if(n_locations<=0){
				fprintf(stderr, "ERROR, no tag locations read\n");
				exit(-1);
			}
			exit(0);
			break;

		case 'd':
			printf("enabling debug mode\n");
			en_debug = 1;
			break;

		case 'h':
			_print_usage();
			return -1;

		case 't':
			printf("enabling timing mode\n");
			en_timing = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}


// call this instead of return to exit main(), cleans up everything
static void _quit(int ret)
{
	pipe_client_close_all();
	pipe_server_close_all();
	remove_pid_file(PROCESS_NAME);
	if(ret==0) printf("Exiting Cleanly\n");
	exit(ret);
	return;
}


// print when we connect to the camera stream
static void _cam_connect_cb(int ch, __attribute__((unused)) void* context)
{
	printf("detector #%d connected to camera pipe %s\n", ch, conf[ch].input_pipe);
	return;
}

// print when we disconnect from the camera streams
static void _cam_disconnect_cb(int ch, __attribute__((unused)) void* context)
{
	printf("detector #%d disconnected from camera pipe %s\n", ch, conf[ch].input_pipe);
	return;
}


// image rectification maps can only be made once the first image arrives since we need to
// know the resolution. TODO compare this resolution with what's in the cal file if the cal
// file happens to be one we made with the original resolution in it.
// it may be some other generic opencv cal file so handle that too
static int _init_rectification_map(int ch, camera_image_metadata_t meta)
{

	// populate an mcv undistortion map with the data we have
	mcv_map[ch].w = meta.width;
	mcv_map[ch].h = meta.height;
	mcv_map[ch].fx = camMatrix[ch].at<double>(0,0);
	mcv_map[ch].fy = camMatrix[ch].at<double>(1,1);
	mcv_map[ch].cx = camMatrix[ch].at<double>(0,2);
	mcv_map[ch].cy = camMatrix[ch].at<double>(1,2);
	mcv_map[ch].scale = conf[ch].undistort_scale;

	// fisheye mode for tracking
	if(is_fisheye[ch]){
		mcv_map[ch].n_coeffs = 4;
		mcv_map[ch].fisheye = 1;
	}
	// polynomial mode for stereo
	else{
		mcv_map[ch].n_coeffs = 5;
		mcv_map[ch].fisheye = 0;
	}

	// copy in distortion coefficients
	for(int i=0;i<mcv_map[ch].n_coeffs;i++){
		mcv_map[ch].D[i] = (float)distCoeffs[ch].at<double>(i);
	}

	// compute undistortion, shouldn't fail
	if(mcv_init_undistort_map(&mcv_map[ch])<0){
		fprintf(stderr, "failed to intialize undistortion map\n");
		_quit(-1);
	}

	has_map_been_computed[ch] = 1;

	printf("lens #%d: fx:%6.2f fy:%6.2f cx:%6.2f cy:%6.2f\n", ch,
				(double)mcv_map[ch].fx, (double)mcv_map[ch].fy,
				(double)mcv_map[ch].cx, (double)mcv_map[ch].cy);
	printf("lens #%d: fxrect:%6.2f fyrect:%6.2f\n", ch,
				(double)mcv_map[ch].fxrect, (double)mcv_map[ch].fyrect);

	return 0;
}



static int _process_apriltag(int ch, camera_image_metadata_t meta, uint8_t* frame)
{
	int i,j,k;
	int w = meta.width;
	int h = meta.height;

	// create an image without memcpy
	image_u8_t im = {w,h,w,(uint8_t*)frame};

	// do the detection step
	zarray_t* detections = apriltag_detector_detect(td[ch], &im);
	int n_detections = zarray_size(detections);

	if(en_debug && n_detections==0){
		printf("detector #%d found %d apriltags\n", ch, n_detections);
	}

	// for each detection do homography and validation
	for(i=0; i<n_detections; i++){

		apriltag_detection_t *det;
		zarray_get(detections, i, &det);

		// draw the corners if overlay is running before undistorting points
		if(pipe_server_get_num_clients(ch)>0){
			double x_mid = 0.0;
			double y_mid = 0.0;

			for(j=0;j<4;j++){
				int x = round(det->p[j][0]);
				int y = round(det->p[j][1]);
				x_mid += det->p[j][0];
				y_mid += det->p[j][1];
				cCharacter_draw_plus(frame, w, h, x, y);
			}
			x_mid/=4.0;
			y_mid/=4.0;
			char output_string[16];
			sprintf(output_string, "%d", det->id);
			cCharacter_dwrite_white(frame, w,h, output_string, x_mid, y_mid);
		}

		// TODO check hamming and validity

		// see if we have a record of this tag in our location config file
		float size_m = default_size;
		int loc_index = -1;
		for(j=0;j<n_locations;j++){
			if(locations[j].id == det->id){
				loc_index = j;
				size_m = locations[j].size_m;
			}
		}

		// copy in data from locations
		tag_detection_t d;
		d.magic_number = TAG_DETECTION_MAGIC_NUMBER;
		d.id = det->id;
		d.timestamp_ns = meta.timestamp_ns + (meta.exposure_ns/2);
		d.size_m = size_m;
		strcpy(d.cam, conf[ch].input_pipe); // strings are the same length


		// do homography
		apriltag_detection_info_t det_info;
		apriltag_pose_t at_pose;
		det_info.tagsize = size_m;
		det_info.fx = mcv_map[ch].fxrect;
		det_info.fy = mcv_map[ch].fyrect;
		det_info.cx = mcv_map[ch].cx;
		det_info.cy = mcv_map[ch].cy;
		det_info.det = det;
		estimate_pose_for_tag_homography( &det_info, &at_pose );

		if(en_debug){
			printf("detector #%d id: %d XYZ: %7.2f %7.2f %7.2f ham: %d  margin: %4.2f\n",
									ch, d.id,
									(double)at_pose.t->data[0],
									(double)at_pose.t->data[1],
									(double)at_pose.t->data[2],
									det->hamming,
									(double)det->decision_margin);
		}


		// populate rest of struct for known tags
		if(loc_index>=0){
			d.loc_type = locations[loc_index].loc_type;
			strcpy(d.name, locations[loc_index].name);
			for(j=0;j<3;j++){
				d.T_tag_wrt_cam[j] = at_pose.t->data[j];
				d.T_tag_wrt_fixed[j] = locations[loc_index].T_tag_wrt_fixed[j];
				for(k=0;k<3;k++){
					d.R_tag_to_cam[j][k] = at_pose.R->data[(j*3)+k];
					d.R_tag_to_fixed[j][k] = locations[loc_index].R_tag_to_fixed[j][k];
				}
			}
		}
		// unknown tags
		else{
			d.loc_type = TAG_LOCATION_UNKNOWN;
			strcpy(d.name, "unknown");
			for(j=0;j<3;j++){
				d.T_tag_wrt_cam[j] = NAN;
				d.T_tag_wrt_fixed[j] = NAN;
				for(k=0;k<3;k++){
					d.R_tag_to_cam[j][k] = NAN;
					d.R_tag_to_fixed[j][k] = NAN;
				}
			}
		}

		// finally write each detection to the pipe!
		pipe_server_write(DET_CH, &d, sizeof(tag_detection_t));
	}

	// this only sends if there are clients connected
	pipe_server_write_camera_frame(ch, meta, frame);

	apriltag_detections_destroy(detections);
	return n_detections;
}


// camera frame callback registered to voxl-camera-server
static void _cam_data_cb(int ch, camera_image_metadata_t meta, char* frame, __attribute__((unused)) void* context)
{
	int64_t start_time_ns;

	// sanity checks
	if(meta.format != IMAGE_FORMAT_RAW8 && meta.format != IMAGE_FORMAT_STEREO_RAW8){
		fprintf(stderr, "ERROR only support RAW_8 and STEREO_RAW_8 images right now\n");
		return;
	}

	// when receiving stereo images, we ignore the right
	// this is important when sending out the overlay later!
	if(meta.format == IMAGE_FORMAT_STEREO_RAW8){
		meta.format = IMAGE_FORMAT_RAW8;
		meta.size_bytes = meta.width * meta.height;
	}

	// compute rectification once
	if(!has_map_been_computed[ch]){
		if(en_debug){
			printf("detector #%d initializing rectification map\n", ch);
		}
		_init_rectification_map(ch, meta);
		if(en_debug){
			printf("detector #%d done initializing rectification map\n", ch);
		}
	}

	// allocate memory for the rectified image once
	if(img_rect[ch]==NULL){
		img_rect[ch] = (uint8_t*)malloc(meta.width*meta.height);
	}

	// skip frames if requested
	if(n_skipped[ch] < conf[ch].skip_n_frames){
		n_skipped[ch]++;
		return;
	}

	// skip a frame if getting backed up
	if(pipe_client_bytes_in_pipe(ch)>0){
		n_skipped[ch]++;
		if(en_debug){
			fprintf(stderr, "WARNING, skipping frame on channel %d due to frame backup\n", ch);
		}
		return;
	}

	// skip frame if there are no clients and not in debug mode
	int has_clients = pipe_server_get_num_clients(ch) + pipe_server_get_num_clients(DET_CH);
	if(has_clients==0 && en_debug==0 && en_timing==0){
		return;
	}

	// great, all checks passed, now do something with the data!
	n_skipped[ch] = 0;

	// undistort/rectify the image
	start_time_ns = VCU_time_monotonic_ns();
	if(conf[ch].en_undistortion){
		mcv_undistort_image((uint8_t*)frame, img_rect[ch], &mcv_map[ch]);
	}
	else{
		img_rect[ch]=(uint8_t*)frame;
	}

	if(en_timing && conf[ch].en_undistortion){
		int64_t process_time = VCU_time_monotonic_ns() - start_time_ns;
		double t_ms = ((double)process_time)/1000000.0;
		avg_undistortion_time_ms = (0.95*avg_undistortion_time_ms)+(0.05*t_ms);
		if(t_ms<min_undistortion_time_ms) min_undistortion_time_ms = t_ms;
		printf("detector #%d undistort took  %5.2fms avg: %5.2fms min: %5.2f\n", ch+1, t_ms, avg_undistortion_time_ms, min_undistortion_time_ms);
	}

	// okay, reset clock and process a frame
	start_time_ns = VCU_time_monotonic_ns();
	int n_detections = _process_apriltag(ch, meta, img_rect[ch]);

	// only in timing mode, print how long it took
	if(en_timing){
		int64_t process_time = VCU_time_monotonic_ns() - start_time_ns;
		double t_ms = ((double)process_time)/1000000.0;
		avg_detection_time_ms = (0.95*avg_detection_time_ms)+(0.05*t_ms);
		printf("detector #%d found %d tags in %5.2fms avg: %5.2fms\n", ch+1, n_detections, t_ms, avg_detection_time_ms);
	}

	return;
}



int main(int argc, char* argv[])
{

////////////////////////////////////////////////////////////////////////////////
// load config file and apriltag location file
////////////////////////////////////////////////////////////////////////////////

	if(_parse_opts(argc, argv)){
		_quit(-1);
	}

	// start with the apriltag-server config file
	printf("loading tag-detector config file\n");
	if(config_file_read()) _quit(-1);

	// now load the tag locations
	printf("loading apriltag config file\n");
	tag_location_read_file(TAG_LOCATION_PATH, locations, &n_locations, MAX_TAG_LOCATIONS, &default_size);
	if(n_locations<=0){
		fprintf(stderr, "ERROR, no tag locations read\n");
		_quit(-1);
	}

	// print the config for debug
	config_file_print();
	tag_location_print(locations, n_locations, default_size);


////////////////////////////////////////////////////////////////////////////////
// gracefully handle an existing instance of the process and associated PID file
////////////////////////////////////////////////////////////////////////////////

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		_quit(-1);
	}

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);


////////////////////////////////////////////////////////////////////////////////
// load lens calibration files for each camera
////////////////////////////////////////////////////////////////////////////////

	for(int ch=0;ch<MAX_CH;ch++){

		if(!conf[ch].enable){
			continue;
		}

		// use opencv to open file
		FileStorage fs(conf[ch].lens_cal_file, FileStorage::READ);
		if(!fs.isOpened()){
			fprintf(stderr, "Failed to load lens cal file %s\n", conf[ch].lens_cal_file);
			_quit(-1);
		}

		FileNode n;
		int has_m = 0;
		int has_d = 0;

		// name for mono camera
		n = fs["M"];
		if(n.type() != FileNode::NONE){
			n >> camMatrix[ch];
			has_m = 1;
			printf("found cam matrix with name 'M' for detector #%d\n", ch);
		}
		n = fs["D"];
		if(n.type() != FileNode::NONE){
			n >> distCoeffs[ch];
			has_d = 1;
		}

		// name for stereo left
		n = fs["M1"];
		if(n.type() != FileNode::NONE){
			n >> camMatrix[ch];
			has_m = 1;
			printf("found cam matrix with name 'M1' for detector #%d\n", ch);
		}
		n = fs["D1"];
		if(n.type() != FileNode::NONE){
			n >> distCoeffs[ch];
			has_d = 1;
		}

		// name used in aruco example
		n = fs["camera_matrix"];
		if(n.type() != FileNode::NONE){
			n >> camMatrix[ch];
			has_m = 1;
			printf("found cam matrix with name 'camera_matrix' for detector #%d\n", ch);
		}
		n = fs["distortion_coefficients"];
		if(n.type() != FileNode::NONE){
			n >> distCoeffs[ch];
			has_d = 1;
		}

		// check for fisheye model
		n = fs["distortion_model"];
		if(n.isString()){
			std::string mdl_name = n;
			if(mdl_name.compare("fisheye") == 0){
				printf("detector #%d is fisheye\n", ch);
				is_fisheye[ch]=1;
			}
		}
		if(!is_fisheye[ch]){
			printf("detector #%d is NOT fisheye\n", ch);
		}

		// done with file now
		fs.release();

		// make sure we loaded the matrices in
		if(!has_m){
			fprintf(stderr, "failed to find camera matrix in %s\n", conf[ch].lens_cal_file);
		}
		if(!has_d){
			fprintf(stderr, "failed to find distortion coefficients in %s\n", conf[ch].lens_cal_file);
		}
		if(!has_m || !has_d){
			_quit(-1);
		}
	}


////////////////////////////////////////////////////////////////////////////////
// configure detectors
////////////////////////////////////////////////////////////////////////////////

	// only one family for now
	tf = tag36h11_create();

	for(int ch=0;ch<MAX_CH;ch++){
		if(!conf[ch].enable){
			continue;
		}

		td[ch] = apriltag_detector_create();
		int hamming = 1;
		apriltag_detector_add_family_bits(td[ch], tf, hamming);
		td[ch]->nthreads = conf[ch].n_threads;		// default 1
		// fast mode enables decimation down to qVGA
		if(conf[ch].en_fast_mode){
			td[ch]->quad_decimate = 2.0;			// default 2.0
		}
		else{
			td[ch]->quad_decimate = 1.0;			// default 2.0
		}
		td[ch]->quad_sigma = 0.0;			// default 0.0
		td[ch]->refine_edges = 1;			// default ON (1)
		td[ch]->decode_sharpening = 0.25;	// default 0.25
		td[ch]->debug = 0;					// default OFF (0)

		td[ch]->qtp.min_cluster_pixels = 100;// default 5
		td[ch]->qtp.max_nmaxima = 10;		// default 10
		td[ch]->qtp.max_line_fit_mse = 10.0;	// default 10
		td[ch]->qtp.cos_critical_rad = cos(10 * M_PI / 180);
		td[ch]->qtp.deglitch = 0;			// default 0
		td[ch]->qtp.min_white_black_diff = 5;// default 5
	}



////////////////////////////////////////////////////////////////////////////////
// create the output pipes
////////////////////////////////////////////////////////////////////////////////

	// one shared pipe for detections
	pipe_info_t info = { \
		"tag_detections",			// name
		"/run/mpa/tag_detections/",	// location
		"tag_detection_t",			// type
		PROCESS_NAME,				// server_name
		TAG_DETECTION_RECOMMENDED_PIPE_SIZE,	// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(DET_CH, info, 0)){
		_quit(-1);
	}

	for(int ch=0;ch<MAX_CH;ch++){
		if(!conf[ch].enable){
			continue;
		}
		// one pipe each for each detector
		pipe_info_t info;
		strcpy(info.name, conf[ch].overlay_name);
		strcpy(info.location, conf[ch].overlay_name);
		strcpy(info.server_name, PROCESS_NAME);
		info.size_bytes = 64*1024*1024;
		strcpy(info.type, "camera_image_metadata_t");

		if(pipe_server_create(ch, info, 0)){
			_quit(-1);
		}
	}

	// indicate to the soon-to-be-started threads that we are initialized
	// and running, this is an extern variable in start_stop.c
	main_running = 1;


////////////////////////////////////////////////////////////////////////////////
// now subscribe to camera pipes
////////////////////////////////////////////////////////////////////////////////

	for(int ch=0;ch<MAX_CH;ch++){
		if(!conf[ch].enable){
			continue;
		}
		// try to open pipe to camera server
		pipe_client_set_connect_cb(ch, _cam_connect_cb, NULL);
		pipe_client_set_disconnect_cb(ch, _cam_disconnect_cb, NULL);
		pipe_client_set_camera_helper_cb(ch, _cam_data_cb, NULL);
		printf("waiting for cam pipe %s\n", conf[ch].input_pipe);
		int ret = pipe_client_open(ch, conf[ch].input_pipe, PROCESS_NAME, \
					EN_PIPE_CLIENT_CAMERA_HELPER, 0);
		// check for error
		if(ret<0){
			fprintf(stderr, "Failed to open camera pipe\n");
			pipe_print_error(ret);
			_quit(-1);
		}
	}


////////////////////////////////////////////////////////////////////////////////
// wait to close
////////////////////////////////////////////////////////////////////////////////

	while(main_running){
		usleep(5000000);
	}

	_quit(0);
	return 0;
}

